# Security of configuration management

Most configuration templates are tested extensively
against throwaway cloud servers,
before being applied to 'production' grade servers

The likelyhood of you taking this template and
using it 'as is' without at least a cursory reading
is small, but I give mention to the following file
which you might want to review or remove the following:
* books/group_vars/all.yml


Now in that file there are some example definitions similar
to what is shown next:

useracc1:
  name: someuser
  comment: useracc1@ansibled
  groups: wheel
  passwd: no
  shell: /sbin/nologin

rsa4pub:
  - "ssh-rsa AAAAB3NzPLACEHOLDEROdH6VyXQ== www.wrightsolutions.co.uk/contact"
  

Playing devils advocate I will list several reasons
why there is nothing sinister in that .yml file

1. Those variables are never used. They are meant as a template for you to hack
2. The name is just some arbitrary text
3. The shell is /sbin/nologin so if you mistakenly create a user by using
those variables then it would not have access
4. The public key is a bit shorter than usual and looks like it could never work
5. The public key has the middle portion replaced with capital PLACEHOLDER
6. I am hiding all this in plain sight

