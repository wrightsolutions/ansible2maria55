# Placeholder files / repo structure

My choice of how to organise Ansible repositories
means I make use of prepopulated structure
and placeholder files.

For adding postfix tasks you might begin
by copying roles/debug or roles/locale
or instead mkdir and edit
- mkdir -p roles/postfix/tasks
- ed roles/postfix/tasks

The intention of prepopulation is to act as a guide
for suggested structure similar to what is shown below.

./host_vars  
./wee.yml  
./group_vars  
./group_vars/Debian.yml  
./group_vars/all.yml  
./group_vars/CentOS.yml  
./group_vars/dashes.txt  
./site.yml  
./site_vars.yml  
./roles  
./roles/debian8  
./roles/debug  
./roles/debug/templates  
./roles/debug/templates/groups-timestamped.j2  
./roles/debug/vars  
./roles/debug/vars/dashes.txt  
./roles/debug/tasks  
./roles/debug/tasks/main.yml  
./roles/debug/files  
./roles/debug/files/dashes.txt  
./roles/debug/handlers  
./roles/debug/handlers/main.yml  
./roles/centos7  
./roles/locale  
./roles/locale/templates  
./roles/locale/templates/dashes.txt  
./roles/locale/vars  
./roles/locale/vars/dashes.txt  
./roles/locale/tasks  
./roles/locale/tasks/main.yml  
./roles/locale/files  
./roles/locale/files/localeGB  
./roles/locale/files/empty.txt  
./roles/locale/handlers  
./roles/locale/handlers/main.yml  
./README.md  

