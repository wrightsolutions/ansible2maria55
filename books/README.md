# Ubuntu LTS

## MySQL 5 settings and permissions

General log is defined but not enabled by default

Set the variable bugsword in role 'bugzilla5' to be the password
you want to use for your bugzilla mysql user

( File hint: roles/bugzilla5/vars/main.yml )

The bugsword variable has a default value of placeholder or similar
which should be replaced with your own secure password

This secure password can be set higher up in Ansible (recommended)  
See Ansible documentation for more detail about this
and other security considerations.


## MySQL 5 granular permissions

The MySQL interface from Ansible is basic but functional

Here in this automation we use a broad brush at first ( GRANT ALL )
to get things working, but provide a helper file also.

On the server in /var/lib/mysql there will be a file created named:  
```mysqluser_bugs.sql```

That sql file can be run using the following command on the server:
```mysql mysql < /var/lib/mysql/mysqluser_bugs.sql```

If the .sql file fails with "There is no such grant defined for user 'bugs'"
then the broad brush permissions mentioned above have not been created

( File hint: roles/bugzilla5/tasks/bugzilla5config.yml )

## Limits of automation / post play configuration required

The aim of this automation is to perform most of the package
installs and module configuration, rather than you having
to do that manually.

It also provides some helpers ( .sql ) and some suggested settings

There is still some manual configuration required once the playbook
has completed, and that is detailed next.

Once the playbook has completed you should have in /var/www/shtml/
a file named 'localconfig'

In 'localconfig' set the variables $db_pass to be your mysql password
which in automation will have been given by 'bugsword' variable
$db_pass = 'placeholder5';

( You will probably have chosen your own password and placed it in
vars/main.yml entry bugsword so use that instead of the default )

In 'localconfig' set the variables $webservergroup to be 'www-data'
as we are a Debian/Ubuntu server

```cd /var/www/shtml
perl ./checksetup.pl
```

If all goes as expected then the output from the above should
include 'Precompiling templates...done'
and prompt you for an email address

Now you should be able to visit the following url and see a bugzilla home page
```http://aa.bb.cc.dd:8380/```

( Where aa.bb.cc.dd is your server IPV4 address )

If you have manually changed the port by entering a different number for cgiport
variable in automation then use that instead of 8380 above

( File hint: roles/apache24/vars/main.yml )

## Feedback / common errors

If you visit the url shown in previous section and see a complaint about data/params
with the message 'You probably need to run checksetup.pl'  
then you need to follow the instructions above or try the automation again
on a clean install server then do the post play configuration steps again

## Bugzilla onscreen parameters - post configuration

http://aa.bb.cc.dd:8380/editparams.cgi

Here you will be prompted to enter 'urlbase' which should be the stem
that you wish to appear in links sent out in emails

Enter http://aa.bb.cc.dd:8380/ if you are not sure

( Where aa.bb.cc.dd is your server IPV4 address )


## Bugzilla mail sending - editparams.cgi?section=mta

Here you should put any non-standard smtp settings local
to your configuration.

Is your server sending mail via Exim or Postfix or something else?


